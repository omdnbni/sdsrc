"use strict";

const {expect} = require("code");

const Boom = require("boom");

const DAO        = require("../lib/DAO");
const DataSource = require("../lib/DataSource");

const U = require("./util");

describe("DAO", function () {

  describe("DAO.statics", function () {

    it("should return a valid schema definition", function () {
      expect(DAO.schemas).to.equal(require("../lib/schemas"));
    });

    it("should return a valid PRNG ", function () {
      const Random = require("random-js");
      expect(DAO.random).to.be.an.instanceof(Random);
    });

    describe("DAO.parseListContext", function () {

      it("should fail without an argument", async function () {
        const boom = expect(() => {
          DAO.parseListContext();
        }).to.throw(Boom, `Unexpected token u in JSON at position 0`);
        expect(boom.output.statusCode).to.equal(400);
      });

      it("should fail with a number argument", async function () {
        const boom = expect(() => {
          DAO.parseListContext(1);
        }).to.throw(Boom, `"value" must be an object`);
        expect(boom.output.statusCode).to.equal(400);
      });

      it("should fail with an array argument", async function () {
        const boom = expect(() => {
          DAO.parseListContext([]);
        }).to.throw(Boom, `Unexpected end of JSON input`);
        expect(boom.output.statusCode).to.equal(400);
      });

      it("should fail with an object argument", async function () {
        const boom = expect(() => {
          DAO.parseListContext({});
        }).to.throw(Boom, `Unexpected token o in JSON at position 1`);
        expect(boom.output.statusCode).to.equal(400);
      });

      it("should fail with an invalid argument", async function () {
        const boom = expect(() => {
          DAO.parseListContext("invalid");
        }).to.throw(Boom, `Unexpected token i in JSON at position 0`);
        expect(boom.output.statusCode).to.equal(400);
      });

      it("should fail with an invalid JSON", async function () {
        const boom = expect(() => {
          DAO.parseListContext(`{"json": "string"}`);
        }).to.throw(Boom, `"json" is not allowed`);
        expect(boom.output.statusCode).to.equal(400);
      });

      it("should work with an empty JSON", function () {
        const expected = {};
        expect(DAO.parseListContext(JSON.stringify(expected))).to.be.an.object()
          .and.equal({});
      });

      it("should work with a basic pagination", function () {
        const expected = {p: {l: 0}};
        expect(DAO.parseListContext(JSON.stringify(expected))).to.be.an.object()
          .and.equal(expected);
      });

      it("should work with a basic filtering", function () {
        const expected = {f: {}};
        expect(DAO.parseListContext(JSON.stringify(expected))).to.be.an.object()
          .and.equal(expected);
      });

      it("should work with a basic sorting", function () {
        const expected = {s: []};
        expect(DAO.parseListContext(JSON.stringify(expected))).to.be.an.object()
          .and.equal(expected);
      });

    });

  });

  describe("DAO.constructor", function () {

    let dsrc;
    before(async function () {
      dsrc = await U.getDataSource();
    });

    it("should fail without any arguments", function () {
      const boom = expect(() => {
        new DAO();
      }).to.throw(Boom, `Missing or invalid DAO data source provider`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with a number", function () {
      const boom = expect(() => {
        new DAO(1);
      }).to.throw(Boom, `Invalid DAO options provided`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an empty array", function () {
      const boom = expect(() => {
        new DAO([]);
      }).to.throw(Boom, `Missing or invalid DAO data source provider`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an empty object", function () {
      const boom = expect(() => {
        new DAO({});
      }).to.throw(Boom, `Missing or invalid DAO data source provider`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with missing entity option", async function () {
      const boom = expect(() => {
        new DAO({dsrc});
      }).to.throw(Boom, `Missing or invalid DAO entity option`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with missing entity pk option", async function () {
      const boom = expect(() => {
        new DAO({dsrc, entity: "entity"});
      }).to.throw(Boom, `Missing or invalid DAO entity pk option`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should work with valid options", async function () {
      const dsrc = await U.getDataSource();
      const dao  = new DAO({dsrc, entity: "entity", pk: "id_entity"});
      expect(dao).to.be.an.instanceof(DAO);
      expect(dao).to.be.an.instanceof(require("events").EventEmitter);
    });

  });

  describe("DAO.properties", function () {

    let dao;
    const entity = "entity", pk = "id_entity";
    before(async function () {
      dao = new DAO({dsrc: await U.getDataSource(), entity, pk});
    });

    it("should return the appropriate entity", function () {
      expect(dao.entity).to.equal(entity);
    });

    it("should return the appropriate pk", function () {
      expect(dao.pk).to.equal(pk);
    });

    it("should return the appropriate dsrc", function () {
      expect(dao.dsrc).to.be.an.instanceof(DataSource);
    });

  });

  describe("DAO.methods", function () {

    let dao;
    const entity = "entity", pk = "id_entity";
    before(async function () {
      dao = new DAO({dsrc: await U.getDataSource(), entity, pk});
    });

    it("should fail to make as it is an abstract class", async function () {
      const boom = await expect(dao.make())
        .to.reject(Boom, `Not implemented for ${entity}.${pk} with undefined and {}`);
      expect(boom.output.statusCode).to.equal(501);
    });

    it("should fail to list as it is an abstract class", async function () {
      const boom = await expect(dao.list())
        .to.reject(Boom, `Not implemented for ${entity}.${pk} with undefined and {}`);
      expect(boom.output.statusCode).to.equal(501);
    });

    it("should fail to load as it is an abstract class", async function () {
      const boom = await expect(dao.load())
        .to.reject(Boom, `Not implemented for ${entity}.${pk} with undefined and {}`);
      expect(boom.output.statusCode).to.equal(501);
    });

    it("should fail to kill as it is an abstract class", async function () {
      const boom = await expect(dao.kill())
        .to.reject(Boom, `Not implemented for ${entity}.${pk} with undefined and {}`);
      expect(boom.output.statusCode).to.equal(501);
    });

    it("should fail to edit as it is an abstract class", async function () {
      const boom = await expect(dao.edit())
        .to.reject(Boom, `Not implemented for ${entity}.${pk} with undefined and {}`);
      expect(boom.output.statusCode).to.equal(501);
    });

  });

});