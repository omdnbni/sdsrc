"use strict";

const {expect} = require("code");

const Boom = require("boom");

const DataSourceBuilder = require("../lib");

const U = require("./util");

describe("DataSourceBuilder", function () {

  describe("DataSourceBuilder.DAOSchemas", function () {

    it("should return the base implementation", function () {
      expect(DataSourceBuilder.DAOSchemas).to.equal(require("../lib/schemas"));
    });

  });

  describe("DataSourceBuilder.getImplementation", function () {

    it("should fail without any arguments", function () {
      const boom = expect(() => {
        DataSourceBuilder.getImplementation();
      }).to.throw(Boom, `String expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with a number", function () {
      const boom = expect(() => {
        DataSourceBuilder.getImplementation(1);
      }).to.throw(Boom, `String expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an empty array", function () {
      const boom = expect(() => {
        DataSourceBuilder.getImplementation([]);
      }).to.throw(Boom, `String expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an empty object", function () {
      const boom = expect(() => {
        DataSourceBuilder.getImplementation({});
      }).to.throw(Boom, `String expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an unsupported implementation", function () {
      const boom = expect(() => {
        DataSourceBuilder.getImplementation("unsupported");
      }).to.throw(Boom, `Unsupported implementation: unsupported`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should work with a valid implementation", function () {
      U.expectImplementation(DataSourceBuilder.getImplementation("couch"));
    });

  });

  describe("DataSourceBuilder.couch", function () {

    it("should return the CouchDB implementation", function () {
      U.expectImplementation(DataSourceBuilder.couch);
    });

  });

  describe("DataSourceBuilder.build", function () {

    it("should fail without any arguments", async function () {
      const boom = await expect(DataSourceBuilder.build())
        .to.reject(Boom, `Object expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with a number", async function () {
      const boom = await expect(DataSourceBuilder.build(1))
        .to.reject(Boom, `Object expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with a string", async function () {
      const boom = await expect(DataSourceBuilder.build("string"))
        .to.reject(Boom, `Object expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an array", async function () {
      const boom = await expect(DataSourceBuilder.build([]))
        .to.reject(Boom, `String expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an empty object", async function () {
      const boom = await expect(DataSourceBuilder.build({}))
        .to.reject(Boom, `String expected`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an unsupported kind", async function () {
      const boom = await expect(DataSourceBuilder.build({kind: "unsupported"}))
        .to.reject(Boom, `Unsupported implementation: unsupported`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with a supported kind w/o options", async function () {
      const boom = await expect(DataSourceBuilder.build({kind: "couch"}))
        .to.reject(Boom, `Invalid database name: undefined`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should work with a supported kind and default server url", async function () {
      const dsrc = await DataSourceBuilder.build({kind: "couch", dbName: "sdsrc"});
      U.expectDataSource(dsrc);
    });

  });

});