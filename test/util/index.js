"use strict";

const {expect} = require("code");

const sdsrc = require("../../lib");

const DataSource = require("../../lib/DataSource");
const DAO        = require("../../lib/DAO");
const DAOSchemas = require("../../lib/schemas");

class Utility {

  static async getDataSource() {
    return sdsrc.build({kind: "couch", dbName: "database"});
  }

  static expectImplementation(implementation) {
    expect(implementation).to.be.an.object()
      .and.only.include(["DataSource", "DAO", "DAOSchemas"]);
    expect(implementation.DataSource.__proto__).to.equal(DataSource);
    expect(implementation.DAO.__proto__).to.equal(DAO);
    expect(implementation.DAOSchemas).to.equal(DAOSchemas);
  }

  static expectDataSource(dsrc) {
    expect(dsrc).to.be.an.instanceof(DataSource);
  }

}

module.exports = Utility;