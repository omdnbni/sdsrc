"use strict";

const {expect} = require("code");

const Boom = require("boom");

const DataSource = require("../lib/DataSource");

describe("DataSource", function () {

  describe("DataSource.statics", function () {

    it("should fail as it is an abstract class", async function () {
      const boom = expect(() => {
        DataSource.DAO;
      }).to.throw(Boom, `Abstract class. Must be overridden`);
      expect(boom.output.statusCode).to.equal(500);
    });

  });

  describe("DataSource.constructor", function () {

    it("should fail without any arguments", function () {
      const boom = expect(() => {
        new DataSource();
      }).to.throw(Boom, `Invalid argument passed: undefined`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with a number", function () {
      const boom = expect(() => {
        new DataSource(1);
      }).to.throw(Boom, `Invalid argument passed: number`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an empty array", function () {
      const boom = expect(() => {
        new DataSource([]);
      }).to.throw(Boom, `Invalid implementation: undefined`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an empty object", function () {
      const boom = expect(() => {
        new DataSource({});
      }).to.throw(Boom, `Invalid implementation: undefined`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should fail with an unsupported implementation and a missing dbName", function () {
      const boom = expect(() => {
        new DataSource({kind: "unsupported"});
      }).to.throw(Boom, `Invalid database name: undefined`);
      expect(boom.output.statusCode).to.equal(500);
    });

    it("should work with a unsupported implementation", function () {
      const dsrc = new DataSource({kind: "unsupported", dbName: "database"});
      expect(dsrc).to.be.an.instanceof(DataSource);
    });

  });

  describe("DataSource.properties", function () {

    const kind   = "unsupported";
    const dbName = "database";

    const dsrc = new DataSource({kind, dbName});

    it("should return appropriate kind", function () {
      expect(dsrc.kind).to.equal(kind);
    });

    it("should return appropriate name", function () {
      expect(dsrc.name).to.equal(dbName);
    });

    it("should return appropriate defaults", function () {
      expect(dsrc.defaults).to.equal({});
    });

    it("should fail to return db as it is an abstract class", function () {
      const boom = expect(() => {
        dsrc.db;
      }).to.throw(Boom, `Not implemented for ${kind} at database with ${dbName}`);
      expect(boom.output.statusCode).to.equal(501);
    });

  });

  describe("DataSource.methods", function () {

    const kind   = "unsupported";
    const dbName = "database";

    const dsrc = new DataSource({kind, dbName});


    it("should fail to setup as it is an abstract class", async function () {
      const boom = await expect(dsrc.setup())
        .to.reject(Boom, `Not implemented for ${kind} at ${dbName} with undefined`);
      expect(boom.output.statusCode).to.equal(501);
    });

    it("should fail to truncate as it is an abstract class", async function () {
      const boom = await expect(dsrc.truncate())
        .to.reject(Boom, `Not implemented for ${kind} at ${dbName} with {}`);
      expect(boom.output.statusCode).to.equal(501);
    });

    it("should fail to plant as it is an abstract class", async function () {
      const boom = await expect(dsrc.plant())
        .to.reject(Boom, `Not implemented for ${kind} at ${dbName} with undefined`);
      expect(boom.output.statusCode).to.equal(501);
    });

    describe("DataSource.getDAO", function () {

      it("should fail to getDAO with invalid options", function () {
        const boom = expect(() => {
          dsrc.getDAO();
        }).to.throw(Boom, `Invalid options provided`);
        expect(boom.output.statusCode).to.equal(500);
      });

      it("should fail to getDAO as it is an abstract class", async function () {
        const boom = expect(() => {
          dsrc.getDAO("entity");
        }).to.throw(Boom, `Abstract class. Must be overridden`);
        expect(boom.output.statusCode).to.equal(500);
      });


    });

    it("should work when clearing DAO cache", async function () {
      dsrc.clear();
      expect(dsrc._dao.size).to.equal(0);
    });

  });

});