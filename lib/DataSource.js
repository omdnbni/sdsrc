"use strict";

const Boom = require("boom");
const Hoek = require("hoek");

const {EventEmitter} = require("events");

/**
 * @class
 */
class DataSource extends EventEmitter {

  /**
   * The DataSource DAO class
   * @constructor
   */
  static get DAO() {
    throw Boom.badImplementation(`Abstract class. Must be overridden`);
  }

  /**
   * A Reachability checker that tries to reach the DataSource
   * making a given number of attempts
   * with a provided delayed between attempts
   *
   * @param {Object} options - DataSource options
   * @param {Number} [tries] - Number of tries
   * @param {Number} [delay] - Number of milliseconds to delay between tries
   * @returns {Promise<*>} If false the DataSource doesn't support this feature
   */
  static async wait(options, tries = 10, delay = 1000) {
    return 0 < tries && 0 < delay;
  }

  /**
   * A DataSource factory
   *
   * @param {Object} options - The DataSource instantiation options
   * @param {Number} [tries] - Number of tries
   * @param {Number} [delay] - Number of milliseconds to delay between tries
   * @returns {Promise<DataSource>}
   */
  static async build(options, tries = 10, delay = 1000) {
    const {DataSource} = require(`./${options.kind}`);
    await DataSource.wait(options, tries, delay);
    const dataSource = new DataSource(options);
    await dataSource.setup();
    return dataSource;
  }

  /**
   * Instantiates the DataSource
   * @param {Object} options
   * @param {String} options.kind
   * @param {String} options.dbName
   * @param {Object} [options.defaults]
   */
  constructor(options) {
    super();
    Hoek.assert(typeof options === "object", Boom.internal(`Invalid argument passed: ${typeof options}`));
    Hoek.assert(typeof options.kind === "string", Boom.internal(`Invalid implementation: ${typeof options.kind}`));
    Hoek.assert(typeof options.dbName === "string", Boom.internal(`Invalid database name: ${typeof options.dbName}`));
    Object.defineProperties(this, {
      /**
       * A map of DataAccessObject that the DataSource has lazily instantiated
       * @type {Map}
       * @private
       */
      _dao     : {value: new Map()},
      /**
       * The kind of DataSource
       * @type {String}
       * @private
       */
      _kind    : {value: options.kind},
      /**
       * The database name
       * @type {String}
       * @private
       */
      _name    : {value: options.dbName},
      /**
       * The default options
       * @type {Object}
       * @private
       */
      _defaults: {value: options.defaults || {}},
    });
    /**
     * The Database connector objects
     * @type {*}
     * @private
     */
    this._db = undefined;
  }

  /**
   * The DataSource discriminator
   * @returns {String}
   */
  get kind() {
    return this._kind;
  }

  /**
   * The DataSource database name
   * @returns {String}
   */
  get name() {
    return this._name;
  }

  /**
   * The default options
   * @returns {Object}
   */
  get defaults() {
    return this._defaults;
  }

  /**
   * Instantiates a DataSource database connector
   * @param {String} dbName the database name
   * @private
   */
  _instantiateDB(dbName) {
    throw Boom.notImplemented(`Not implemented for ${this._kind} at ${this._name} with ${[dbName].join()}`);
  }

  /**
   * The DataSource database connector
   * @returns {*}
   */
  get db() {
    if(!this._db) {
      this._db = this._instantiateDB(this._name);
    }
    return this._db;
  }

  /**
   * Clears the DAO cache
   * @returns {Promise<void>}
   */
  async clear() {
    this._db = undefined;
    this._dao.clear();
  }

  /**
   * Returns a requested DAO and lazily instantiates it if necessary
   *
   * @param {Object|String} opts
   * @param {String}        opts.entity
   * @param {String}        [opts.pk]
   * @param {Object}        [opts.defaults]
   * @param {Function<DAO>} [opts.DAO]
   * @param {Boolean}       [rebuild = false]
   * @returns {Promise<CouchDAO>}
   */
  getDAO(opts, rebuild = false) {
    if(typeof opts === "string") {
      opts = {entity: opts};
    }
    if(typeof opts !== "object") {
      throw Boom.badImplementation(`Invalid options provided`);
    }
    let {entity, pk, DAO, defaults = this.defaults} = opts;
    if(rebuild || !this._dao.has(entity)) {
      pk = pk || `id_${entity}`;
      if(!DAO) {
        DAO = this.constructor.DAO;
      }
      const dao = new DAO({entity, pk, defaults, dsrc: this.db});
      ["silly", "debug", "info", "error", "warning"].forEach((event) => {
        dao.on(event, function () {
          this.emit(event, ...arguments);
        }.bind(this));
      });
      this._dao.set(entity, dao);
    }
    return this._dao.get(entity);
  }

  /**
   * Truncates the Database
   * @param {Object} [options = {}]
   * @returns {Promise<*>}
   */
  async truncate(options = {}) {
    throw Boom.notImplemented(`Not implemented for ${this._kind} at ${this._name} with ${JSON.stringify(options)}`);
  }

  /**
   * Sets the database up
   * @param {Object} [options]
   * @returns {Promise<*>}
   */
  async setup(options) {
    throw Boom.notImplemented(`Not implemented for ${this._kind} at ${this._name} with ${JSON.stringify(options)}`);
  }

  /**
   * Plants the provided data into the database
   * @param {*} [seed]
   * @returns {Promise<*>}
   */
  async plant(seed) {
    throw Boom.notImplemented(`Not implemented for ${this._kind} at ${this._name} with ${JSON.stringify(seed)}`);
  }

}

module.exports = DataSource;