"use strict";

const Joi = require("joi");

/**
 * ListContext Pager definition
 *
 * Used to define list response pagination
 *
 * @typedef {Object} ListPager
 * @property [Number] l - The pagination limit. If not set default will be used
 * @property [*]      c - The pagination cursor. If set offset is ignored
 * @property [Number] o - The pagination offset. If not set default will be used
 */
const P_L = Joi.number().integer().min(0)
  .description("Pager Frame Limit");
const P_O = Joi.number().integer().min(0)
  .description("Pager Frame Offset");
const P_C = Joi.string()
  .description("Pager Frame Cursor");

/**
 * @readonly
 * @const {ListPager}
 */
const ListPager = Joi.object().keys({
  l: P_L.optional(),
  o: P_O.optional(),
  c: P_C.optional(),
}).nand("o", "c")
  .description("ListContext Pager Descriptor")
  .meta({className: "ListPager", description: "ListContext Pager Descriptor"})
;
/**
 * ListContext Filter definition
 *
 * Used to define a collection of filters to apply to the list response
 *
 * @typedef {Object} ListFilter
 */

/**
 * @readonly
 * @const {ListFilter}
 */
const ListFilter = Joi.object()
  .description("ListContext Filter Descriptor")
  .meta({className: "ListFilter", description: "ListContext Filter Descriptor"})
;

/**
 * ListContext Sorter definition
 *
 * Used to define the list response sort order by field
 *
 * @typedef {Object[]} ListSorter
 * @property {String} ListSorter[].f - the entity field
 * @property {String} ListSorter[].d - the sorting direction enum
 */
const S_F = Joi.string()
  .description("Sort Field");
const S_D = Joi.string().valid("ASC", "DESC")
  .description("Sort Direction");

/**
 * @readonly
 * @const {ListSorter}
 */
const ListSorter = Joi.array().items(Joi.object().keys({
  f: S_F.required(),
  d: S_D.required(),
}))
  .description("ListContext Sorter Descriptor")
  .meta({className: "ListSorter", description: "ListContext Sorter Descriptor"})
;
/**
 * List Context definition
 *
 * @typedef {Object} ListContext
 * @property {ListPager}  p - The ListContext Pager descriptor
 * @property {ListFilter} f - The ListContext Filter descriptor
 * @property {ListSorter} s - The List
 */

/**
 * @readonly
 * @const {ListContext}
 */
const ListContext = Joi.object().keys({
  p: ListPager.optional(),
  f: ListFilter.optional(),
  s: ListSorter.optional(),
})
  .description("ListContext Descriptor")
  .meta({className: "ListSorter", description: "ListContext Descriptor"})
;

/**
 * JSONed List Context definition
 *
 * Used to pass the ListContext as a JSON string over a HTTP request query string
 *
 * @typedef {String} JSONedListContext
 */

/**
 * DAO Joi Schemas
 *
 * @readonly
 * @enum{{list: ListContext, pager: ListPager, filter: ListFilter, sorter: ListSorter, context: }}
 */
class DAOSchemas {

  static get context() {
    return ListContext;
  }

  /* istanbul ignore next */
  static get filter() {
    return ListFilter;
  }

  /* istanbul ignore next */
  static get sorter() {
    return ListSorter;
  }

  /* istanbul ignore next */
  static get pager() {
    return ListPager;
  }

}

module.exports = DAOSchemas;