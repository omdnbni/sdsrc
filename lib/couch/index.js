"use strict";

module.exports = {
  DataSource: require("./CouchDataSource"),
  DAO       : require("./CouchDAO"),
};