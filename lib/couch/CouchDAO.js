"use strict";

const fs   = require("fs");
const Boom = require("boom");
const Hoek = require("hoek");

const fs2b = require("fast-stream-to-buffer");

const Stream   = require("stream");
const {Buffer} = require("buffer");

const DAO = require("../DAO");

/**
 * @typedef {Object} CouchLock
 * @property {JSTimestamp} _created  - The timestamp when the lock was created
 * @property {Number}      _options  - The options with which the lock was created
 * @property {Number}      _timeout  - The timeout handler (setTimeout)
 * @property {Number}      _release  - The auto-release handler (setTimeout)
 * @property {Object}      _docLock  - The raw CouchDB document storing the lock
 * @property {Number}      attempts  - The number of attempts
 * @property {String}      owner     - The lock owner
 * @property {JSTimestamp} obtained  - The timestamp when the lock was obtained
 * @property {JSTimestamp} released  - The timestamp when the lock was released
 * @property {JSTimestamp} timeouted - The timestamp when the lock was released
 */

/**
 * Parses the provided arguments for the entity ID
 *
 * If not found the function will simply return undefined
 *
 * @param {Object}  args    - Arguments
 * @param {Boolean} strict  - Whether to remove the parsed ID from the provided args argument
 * @returns {*} The entity ID
 * @private
 */
function _parseID(args, strict) {
  if(args.hasOwnProperty(this.pk)) {
    const id = _buildID.call(this, args[this.pk]);
    if(!strict) {
      delete args[this.pk];
    }
    return id;
  }
  return undefined;
}

/**
 * Ensures that an entity ID is built
 *
 * @param {String} id       - A provided entity ID
 * @param {Number} [length] - An expected ID length (hex string)
 * @returns {String}
 * @private
 */
function _buildID(id, length = 32) {
  return id || DAO.random.hex(length);
}

/**
 * @class CouchDAO
 */
class CouchDAO extends DAO {

  /**
   * Creates a new CouchDAO
   *
   * The options should include a Data Source provider eg.
   * KnexJS, Node-CouchDB, Node-MongoDB, ...
   *
   * @constructor
   * @param {{entity: String, pk: String, dsrc: nano.ServerScope} } opts
   */
  constructor(opts) {
    super(opts);
    Object.defineProperties(this, {
      /**
       * @type Array
       * @private
       */
      _locks: {value: []},
    });
  }

  /**
   * Copies a readable stream into a buffer and returns sed buffer and a fresh copy of the original stream.
   *
   * @notes
   * This monstrosity is required since CouchDB's nano doesn't handle streams when inserting multipart data
   * aka attachments, instead requiring us to forcibly shove the stream's content into a Buffer or an Array
   * thus rendering our server catatonic filling the RAM with transitory semantically irrelevant data bytes
   *
   * @param {Readable} stream
   * @returns {Promise<{data:Buffer, copy: PassThrough}>}
   * @private
   */
  static async _copyStream2Buffer(stream) {
    return new Promise((resolve, reject) => {
      fs2b(stream, (err, buffer) => {
        if(err) {
          reject(err);
        }
        resolve(buffer);
      });
    });
  }

  /**
   * Copies the contents of a file into a Buffer
   *
   * @notes
   * This monstrosity is required since CouchDB's nano doesn't handle streams when inserting multipart data
   * aka attachments, instead requiring us to forcibly shove the stream's content into a Buffer or an Array
   * thus rendering our server catatonic filling the RAM with transitory semantically irrelevant data bytes
   *
   * @param {String} path     - The file path
   * @param {String} encoding - The file encoding
   * @return {Promise<*|Buffer>}
   * @private
   */
  static async _copyFile2Buffer(path, encoding = "utf8") {
    return Buffer.from(fs.readFileSync(path), encoding);
  }

  /**
   * Format the attachment into a structure that nano multipart can accept it.
   *
   * @param {Stream|String} upload        - The stream or file path to be uploaded
   * @param {String}        name          - The attachment name
   * @param {String}        content_type  - The attachment content-type
   * @param {String}        [encoding]    - The attachment encoding (defaults to "utf8")
   * @returns {Promise<{data: Buffer, name: String, content_type: String}>}
   */
  async toAttachment(upload, name, content_type, encoding) {
    const data = upload instanceof Stream
      ? await CouchDAO._copyStream2Buffer(upload)
      : await CouchDAO._copyFile2Buffer(upload, encoding || this.opts.encoding)
    ;
    return {
      data,
      name,
      content_type,
    };
  }

  /**
   * Transforms the raw Document into a Entity
   *
   * Removes any CouchDB fields
   *
   * @param {Object}  doc  - The raw CouchDB document
   * @param {Object}  opts - The transformation options
   * @returns {Object}
   * @private
   */
  static _transform(doc, opts = {raw: false}) {
    if(doc && !opts.raw) {
      return this._clear(doc);
    }
    return doc;
  }

  /**
   * Remove all relevant CouchDB metadata from the provided raw document:
   * _id, _rev, _attachments, KIND
   *
   * If an undefined document is provided the function will skip any modifications and return the input
   *
   * @param {Object|undefined} doc - The raw document
   * @returns {Object|undefined}
   * @private
   */
  static _clear(doc) {
    if(doc) {
      delete doc._id;
      delete doc._rev;
      delete doc._attachments;
      delete doc.KIND;
    }
    return doc;
  }

  /**
   * Parses the provided ListContext and build a Mango find context
   * @param {ListFilter} [f]        - A ListContext filtering definition
   * @param {ListSorter} [s]        - A ListContext sorting definition
   * @param {ListPager}  [p]        - A ListContext pagination definition
   * @param {Object}     [selector] - A required filter to apply independent of the ListContext filter definition
   * @returns {{selector: Object, sort:Array, limit:Number, skip:Number, bookmark:* }}
   * @private
   */
  _parseListContext({f, s, p} = {}, selector = {}) {
    const context = {selector: Hoek.merge(selector, {"KIND": this.entity})};

    if(0 < this._defaults.pagination.limit) {
      context.limit = this._defaults.pagination.limit;
    }

    if(f) {
      delete f.KIND;
      Object.assign(context.selector, f);
    }

    if(s && s.length) {
      context.sort = s.map(({d, f}) => ({[f]: d.toLowerCase()}));
    }

    if(p) {
      if(0 < p.l) {
        context.limit = p.l;
      }
      if(0 < p.o) {
        context.skip = p.o;
      }
      if(p.c) {
        context.bookmark = p.c;
      }
    }

    return context;
  }

  _debug(head, context) {
    if(head.warning) {
      this._onWarning(head.warning, context);
    }
    this._onSilly(head, context);
  }

  /**
   * Retrieves a raw document by the provided entity id (not the document id)
   *
   * @param {String}  id              - The DAO Entity ID
   * @param {Boolean} [strict]        - If true it throws a NotFound error, otherwise returns undefined
   * @param {Boolean} [include_docs]  - Whether to include the document body in the response or just the head
   * @param {Boolean} [view]          - The view to request. Defaults to "list"
   * @returns {Promise<nano.DocumentGetResponse>}
   * @private
   */
  async _locate(id, strict = true, include_docs = true, view = "list") {
    if(!id) {
      if(strict) {
        throw Boom.notFound();
      }
      return undefined;
    }
    const list = await this.view(view, {key: id, include_docs});
    this._debug(list, {id, strict, include_docs});
    const {rows} = list;
    if(!rows.length) {
      if(strict) {
        throw Boom.notFound();
      }
      return undefined;
    }
    const [head] = rows;
    return head.value;
  }

  /**
   * Inserts or Modifies a raw document including attachments and transformers
   *
   * @param {Object}    doc           - The document object including {_id, KIND, [this.pk]}
   * @param {Object}    [attachments] - The attachments if any
   * @param {Function}  [transformer] - The document transformer
   * @returns {Promise<nano.DocumentInsertResponse>}
   * @private
   */
  async _multipart(doc, attachments, transformer) {
    let head;
    if(attachments) {
      head = await this.dsrc.multipart.insert(doc, attachments, doc._id);
      this._debug(head, {doc, attachments});
      if(transformer) {
        doc       = await this.dsrc.get(head.id, {rev: head.rev});
        const mod = transformer(doc);
        if(mod) {
          Object.assign(mod, {_id: head.id, _rev: head.rev});
          head = await this.dsrc.insert(mod);
          this._debug(head, {mod});
        }
      }
    } else {
      head = await this.dsrc.insert(doc);
      this._debug(head, {doc});
    }
    return head;
  }

  /**
   * Modifies a raw document including attachments and transformers
   *
   * @param {Object}    doc           - Raw CouchDB document to modify
   * @param {Object}    [attachments] - The attachments if any
   * @param {Function}  [transformer] - The document transformer
   * @returns {Promise<nano.DocumentInsertResponse>}
   * @private
   */
  async _sequential(doc, attachments, transformer) {
    let head = await this.dsrc.insert(doc);
    this._debug(head, {doc});
    if(attachments) {
      for(let {name, data, content_type} of attachments) {
        head = await this.dsrc.attachment.insert(head.id, name, data, content_type, {rev: head.rev});
        this._debug(head, {head, name, content_type});
      }
      if(transformer) {
        doc       = await this.dsrc.get(head.id, {rev: head.rev});
        const mod = transformer(doc);
        if(mod) {
          Object.assign(mod, {_id: head.id, _rev: head.rev});
          head = await this.dsrc.insert(mod);
          this._debug(head, {mod});
        }
      }
    }
    return head;
  }

  /**
   * Builds a "blank" new raw CouchDB document
   * @param {String} id         - The entity ID
   * @param {String} [docName]  - The raw CouchDB document id {doc._id}
   * @param {Object} [body]     - Additional fields to attach to the document
   * @returns {{_id: String, KIND: String, [this.pk]: String, ...body}}
   * @private
   */
  _newDocument(id, docName, body = {}) {
    return Object.assign(body, {_id: _buildID.call(this, docName, 40), KIND: this.entity, [this.pk]: id});
  }

  async make(args, opts = {raw: false, id: undefined}) {
    args = Hoek.clone(args);
    try {
      const id = _buildID.call(this, _parseID.call(this, args));

      let doc, head, {attachments, transformer} = opts;

      doc = this._newDocument(id, _buildID.call(this, opts.id, 40), args);

      head = await this._multipart(doc, attachments, transformer);

      doc = await this.dsrc.get(head.id, {rev: head.rev});

      return {
        data: this.constructor._transform(doc, opts),
        meta: {code: 201},
      };
    } catch(ex) {
      this._onException(ex, {args, opts});
    }
  }

  async list(args, opts = {raw: false, view: "list", selector: {}}) {
    try {
      const context = this._parseListContext(args._, opts.selector);

      const list = await this.dsrc.find(context);
      this._debug(list, {args, opts});

      const head = await this.view(opts.view, {include_docs: false});
      this._debug(head, {args, opts});

      const data = list.docs.map(doc => this.constructor._transform(doc, opts));

      const meta = {
        cursor: list.bookmark,
        count : data.length,
        total : head.total_rows,
      };

      return {
        data,
        meta,
      };
    } catch(ex) {
      this._onException(ex, {args, opts});
    }
  }

  async load(args, opts = {raw: false}) {
    try {
      const id = _parseID.call(this, args, true);

      const doc = await this._locate(id);

      return {
        data: this.constructor._transform(doc, opts),
        meta: {},
      };
    } catch(ex) {
      this._onException(ex, {args, opts});
    }
  }

  async edit(args, opts = {raw: false}) {
    args = Hoek.clone(args);
    try {
      const id = _parseID.call(this, args);

      let doc, head, {attachments, transformer} = opts;

      doc = Object.assign(await this._locate(id), args); // 404 failsafe

      head = await this._multipart(doc, attachments, transformer);

      doc = await this.dsrc.get(head.id, {rev: head.rev});

      return {
        data: this.constructor._transform(doc, opts),
        meta: {},
      };
    } catch(ex) {
      this._onException(ex, {args, opts});
    }
  }

  async kill(args, opts = {raw: false}) {
    try {
      const id = _parseID.call(this, args, true);

      const doc = await this._locate(id); // 404 failsafe

      const head = await this.dsrc.destroy(doc._id, doc._rev);
      this._debug(head, {args, opts});

      return {
        data: this.constructor._transform(doc, opts),
        meta: {affected: 1},
      };
    } catch(ex) {
      this._onException(ex, {args, opts});
    }
  }

  /**
   * Invokes the default entity design document view handler
   *
   * @param {String} name     - View handler name
   * @param {Object} [params] - View handler query string parameters
   * @returns {Promise<{rows:Array}>}
   */
  async view(name, params = {}) {
    const list = await this.dsrc.view(this.entity, name, params);
    this._debug(list, {name, params});
    return list;
  }

  /**
   * Obtains a write lock on an entity using a specified locker CouchDB design document
   *
   * @param {Number} [autoRelease] - For how long the lock should be held before auto releasing in ms
   * @param {Number} [timeout]     - For how long to try to obtain the write lock in ms
   * @param {String} [locker]      - The name of the locker CouchDB design document
   * @returns {Promise<CouchLock>} A lock handle to be used to free the lock
   */
  async lock(autoRelease = 10000, timeout = 10000, locker = "locker") {
    const opts = {autoRelease, timeout, locker};
    return new Promise((resolve, reject) => {
      const lockHandle = {
        _created: Date.now(),
        _options: opts,
        attempts: 0,
      };
      /**
       * Tries to obtain the lock in a loop
       * @param {CouchLock} handle
       * @returns {Boolean} `true` if successful, `false` if timeouted
       */
      const obtainLock = (handle) => {
        if(handle.timeouted) {
          return false;
        }
        handle.attempts++;
        return this.dsrc.atomic(handle._options.locker || "locker", "lock", this.entity)
          .then(doc => {
            clearTimeout(handle._timeout);
            handle.obtained = Date.now();
            handle._docLock = doc;
            handle.owner    = doc.lock;
            handle._release = autoRelease ? setTimeout(lock => {
              this.free(lock).then(
                doc => this._onDebug(doc, {entity: this.entity, opts}),
                err => this._onError(err, {entity: this.entity, opts}),
              );
            }, autoRelease, handle) : undefined;
            this._locks.push(handle);
            resolve(handle);
            return true;
          })
          .catch(ex => {
            if(ex.statusCode !== 409) {
              reject(Boom.boomify(ex, {statusCode: 500}));
              return handle;
            }
            return obtainLock(handle);
          });
      };

      lockHandle._timeout = timeout ? setTimeout((handle) => {
        handle.timeouted = Date.now();
        reject(Boom.internal(`Couldn't obtain a write lock on ${this.entity}`));
      }, timeout, lockHandle) : undefined;

      obtainLock(lockHandle)
        .then(success => this._onDebug(success, {entity: this.entity, opts}))
      ;
    });
  }

  /**
   * Releases the write lock on an entity obtained by `this.lock()` call
   *
   * @param {CouchLock} lock   - The write lock handle
   * @param {Object}    [data] - The write lock meta-data
   * @returns {Promise<boolean>}
   */
  async free(lock, data = {}) {
    if(!lock) {
      lock = this._locks.pop();
    }
    if(typeof lock !== "object") {
      throw Boom.badImplementation(`A lock handle is expected: ${typeof lock}`);
    }
    if(lock._release) {
      clearTimeout(lock._release);
    }
    if(lock.released) {
      return false;
    }
    data.owner = lock.owner;
    await this.dsrc.atomic(lock._options.locker || "locker", "free", this.entity, data);
    lock.released = Date.now();

    const idx = this._locks.indexOf(lock);
    if(0 <= idx) {
      this._locks.splice(idx, 1);
    }
    return true;
  }
}

/**
 * @example Locker CouchDB design document
 * {
 *   method: "PUT",
 *   path: "/<db>/_design/<locker_name>",
 *   body: `{
 *     updates: {
 *       lock  : function (doc, req) {
 *         var data = (!req.body || req.body === "" || req.body === "undefined") ? req.query : JSON.parse(req.body);
 *         if(!doc) {
 *           doc = {_id: data.entity || req.path[req.path.length - 1], last: 0};
 *         }
 *         if(doc.lock) {
 *           return [null, {code: 409}];
 *         }
 *         doc.lock = data.owner || req.uuid;
 *         return [doc, {code: doc._rev ? 200 : 201, json: {_id: doc._id, _rev: doc._rev, last: doc.last, lock: doc.lock}}];
 *       },
 *       unlock: function (doc, req) {
 *         var data = (!req.body || req.body === "" || req.body === "undefined") ? req.query : JSON.parse(req.body);
 *         if(!doc) {
 *           return [null, {code: 404}];
 *         }
 *         if(!data.owner) {
 *           return [null, {code: 401}];
 *         }
 *         if(doc.lock !== data.owner) {
 *           return [null, {code: 403}];
 *         }
 *         delete doc.lock;
 *         if(data.last) {
 *           doc.last = data.last | 0;
 *         }
 *         return [doc, {code: 200, json: {_id: doc._id, _rev: doc._rev, last: doc.last, lock: doc.lock}}];
 *       }
 *     }
 *   }`
 * };
 */

module.exports = CouchDAO;