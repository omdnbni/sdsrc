"use strict";

const http = require("http");
const URL  = require("url").URL;
const Boom = require("boom");
const Hoek = require("hoek");

const nano = require("nano");

const DataSource = require("../DataSource");
const CouchDAO   = require("./CouchDAO");

function _parseURL(url) {
  const base = new URL("http://localhost:5984");
  switch(typeof url) {
    case "string":
      return new URL(url);
    case "object":
      Object.assign(base, url);
  }
  return base;
}

async function _setupDatabase(couch, dbName) {
  try {
    return await couch.db.get(dbName);
  } catch(ex) {
    /* istanbul ignore if */
    if(ex.errid !== "non_200") {
      throw Boom.boomify(ex);
    }
    try {
      return await couch.db.create(dbName);
    } catch(ex) {
      throw Boom.boomify(ex);
    }
  }
}

async function _purgeDatabase(couch, dbName, timeout = 200) {
  try {
    const destroyBody = await couch.db.destroy(dbName);
    // HACK: Dirty fix when CouchDB returns that the database has been destroyed (almost, but not really)
    return 0 < timeout
      ? new Promise(resolve => setTimeout(() => resolve(destroyBody), timeout))
      : destroyBody;
  } catch(error) {
    throw Boom.boomify(error);
  }
}

class CouchDataSource extends DataSource {

  /**
   * The DataSource DAO class
   * @constructor
   */
  static get DAO() {
    return CouchDAO;
  }

  /**
   * A Reachability checker that tries to reach the DataSource
   * making a given number of attempts
   * with a provided delayed between attempts
   *
   * @param {Object} options - DataSource options
   * @param {Number} [tries] - Number of tries
   * @param {Number} [delay] - Number of milliseconds to delay between tries
   * @returns {Promise<*>} If false the DataSource doesn't support this feature
   */
  static async wait(options, tries = 10, delay = 1000) {
    if(!await super.wait(options, tries, delay)) {
      return false;
    }
    const request = _parseURL(options.url);
    const seconds = delay / 1000;

    function reach({hostname, port}, callback) {
      http.get({hostname, port}, (resp) => {
        let body = [];
        resp.on("data", (data) => body.push(data));
        resp.on("end", () => callback(undefined, JSON.parse(body.join(""))));
      }).on("error", callback);
    }

    return new Promise((resolve, reject) => {
      function iterate(count) {
        if(count <= 0) {
          reject(Boom.internal(`Database not reachable after ${tries} tries over the period of ${Math.floor(tries * seconds)}s`));
        } else {
          setTimeout((request) => {
            const tryNo = tries - count + 1;
            console.warn(`Reaching the database for the #${tryNo} time`);
            reach(request, (err, data) => {
              if(err) {
                iterate(count - 1);
              } else {
                console.warn(`Database reached after ${tryNo} tries over the period of ${Math.floor(tryNo * seconds)}s`);
                resolve(data);
              }
            });
          }, delay, request);
        }
      }

      reach(request, (err, data) => {
        if(err) {
          iterate(tries);
        } else {
          resolve(data);
        }
      });
    });
  }

  /**
   * Instantiates the DataSource
   * @param {Object} options
   * @param {String} options.kind
   * @param {String} options.dbName
   * @param {Object|Object} options.url
   * @param {Object} [options.defaults]
   */
  constructor(options) {
    super(options);
    const url   = _parseURL(options.url);
    const couch = nano({...options, url: url.toString()});
    Object.defineProperties(this, {
      _url  : {value: url},
      _base : {value: new URL(this._name, url)},
      _couch: {value: couch},
      _setup: {value: options.setup || {}},
    });
  }

  /**
   * Returns the CouchDB API URL
   * @returns {URL}
   */
  get url() {
    return this._url;
  }

  /**
   * Returns the CouchDB Database API URL
   * @returns {URL}
   */
  get base() {
    return this._base;
  }

  /**
   * Instantiates a DataSource database connector
   * @param {String} dbName the database name
   * @private
   */
  _instantiateDB(dbName) {
    return this._couch.use(dbName);
  }

  /**
   * Truncates the Database
   * @param {Object} [options = {}]
   * @returns {Promise<*>}
   */
  async truncate(options = {}) {
    const {entity, seed, timeout} = options;

    let result = {setup: false, plant: false};

    /* istanbul ignore if */
    if(entity) {
      result.warning = `Truncating a specific entity is not supported: ${entity}`;
      this.emit("warning", {entity, context: options, warning: result.warning});
    }

    await this.clear();
    await _purgeDatabase(this._couch, this._name, timeout);
    result.setup = await this.setup();

    if(seed) {
      result.plant = await this.plant(seed);
    }

    return result;
  }

  /**
   * Sets the database up
   * @param {Object} [options]
   * @returns {Promise<*>}
   */
  async setup(options) {
    const {design = {}, indices = []} = options || this._setup;

    let response, result = {design: false, indices: false};

    await _setupDatabase(this._couch, this._name);

    let keys = Object.keys(design).map(key => `_design/${key}`);

    function _isEqual(cdbDoc, locDoc) {
      for(let type of ["views", "updates", "shows"]) {
        if(!Hoek.deepEqual(cdbDoc[type], locDoc[type])) {
          return false;
        }
      }
      return true;
    }

    response = await this.db.fetch({keys});
    let docs = response.rows
      .map(row => {
        const {doc}   = row;
        const [, key] = row.key.split("/");
        if(!doc || row.error === "not_found") {
          return {_id: row.key, ...design[key]};
        } else {
          if(_isEqual(doc, design[key])) {
            return null;
          }
          return Hoek.applyToDefaults(doc, design[key]);
        }
      })
      .filter(doc => doc)
    ;
    if(docs.length) {
      response = await this.db.bulk({docs});
      let errs = response.filter(row => row.error);
      if(errs.length) {
        throw Boom.internal(`Design documents couldn't be seeded: ${errs.map(({error}) => error).join()}`);
      }
      result.design = docs;
    }

    response     = await this.db.get("_index");
    let existing = response.indexes
      .filter(index => index.type !== "special")
      .map(({name}) => name)
    ;
    let missing  = indices.filter(({name}) => !existing.includes(name));
    if(missing.length) {
      for(let indexDef of missing) {
        response = await this.db.createIndex(indexDef);
        if(response.error) {
          throw Boom.internal(`Index ${indexDef.name} couldn't be created: ${response.error}`);
        }
      }
      result.indices = missing;
    }

    return result;
  }

  /**
   * Plants the provided data into the database
   * @param {*} [seed]
   * @returns {Promise<*>}
   */
  async plant(seed) {
    await this.db.bulk({new_edits: false, docs: seed});
    const draft = seed.filter(({KIND}) => (KIND === "draft"));
    const batch = seed.filter(({KIND}) => (KIND === "batch"));
    const links = batch.reduce((acc, doc) => {
      Object.entries(doc.attachments).forEach(([type, att]) => {
        acc.set(att.hash, [doc._id, type]);
      });
      return acc;
    }, new Map());
    return {
      draft: draft,
      batch: batch,
      ipfs : [...links.values()],
    };

  }

}

module.exports = CouchDataSource;