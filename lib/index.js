"use strict";

const Boom = require("boom");
const Hoek = require("hoek");

const DAOSchemas = require("./schemas");

/**
 * @typedef {Number} JSTimestamp Javascript style timestamp in milliseconds
 */

class DataSourceBuilder {

  /**
   *
   * @returns {DAOSchemas}
   * @constructor
   */
  static get DAOSchemas() {
    return DAOSchemas;
  }

  static get couch() {
    return DataSourceBuilder.getImplementation("couch");
  }

  static getImplementation(kind) {
    Hoek.assert(typeof kind === "string", Boom.badImplementation(`String expected`));
    try {
      return Hoek.applyToDefaults({DAOSchemas}, require(`./${kind}`));
    } catch(ex) {
      /* istanbul ignore else */
      if(ex.code === "MODULE_NOT_FOUND") {
        ex.message = `Unsupported implementation: ${kind}`;
      }
      throw Boom.boomify(ex, {statusCode: 500});
    }
  }

  static async build(options) {
    Hoek.assert(typeof options === "object", Boom.badImplementation(`Object expected`));
    const {DataSource} = DataSourceBuilder.getImplementation(options.kind);
    const dataSource   = new DataSource(options);
    await dataSource.setup();
    return dataSource;
  }
}

module.exports = DataSourceBuilder;