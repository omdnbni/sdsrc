"use strict";

const Joi  = require("joi");
const Boom = require("boom");
const Hoek = require("hoek");

const {EventEmitter} = require("events");

const random = require("random-js")();

const DAOSchemas = require("./schemas");

/**
 * Data Access Object interface
 *
 * @abstract
 * @class
 */
class DAO extends EventEmitter {

  /**
   * Returns a PRNG
   * @returns {Random}
   */
  static get random() {
    return random;
  }

  /**
   * Returns the DAO Joi Schemes
   * @returns {DAOSchemas}
   */
  static get schemas() {
    return DAOSchemas;
  }

  /**
   * Parses and validates the provided JSONedListContext
   * @param {String} json
   * @return {ListContext}
   */
  static parseListContext(json) {
    let lctx;
    try {
      lctx = JSON.parse(json);
    } catch(error) {
      throw Boom.boomify(error, {statusCode: 400, override: false});
    }
    const {error, value} = Joi.validate(lctx, this.schemas.context);
    if(error) {
      throw Boom.boomify(error, {statusCode: 400, override: false});
    }
    return value;
  }

  /**
   * Creates a new DAO
   *
   * The options should include a Data Source provider eg.
   * KnexJS, Node-CouchDB, Node-MongoDB, ...
   *
   * @constructor
   * @param {{entity: String, pk: String, dsrc: *} } opts
   */
  constructor(opts) {
    super();
    opts = opts || {};

    Hoek.assert(typeof opts === "object", Boom.badImplementation("Invalid DAO options provided"));
    Hoek.assert(typeof opts.dsrc === "object", Boom.badImplementation("Missing or invalid DAO data source provider"));
    Hoek.assert(typeof opts.entity === "string", Boom.badImplementation("Missing or invalid DAO entity option"));
    Hoek.assert(typeof opts.pk === "string", Boom.badImplementation("Missing or invalid DAO entity pk option"));

    Object.defineProperties(this, {
      /**
       * @type {Object}
       * @private
       */
      _opts    : {value: opts},
      /**
       * @type {String}
       * @private
       */
      _entity  : {value: opts.entity},
      /**
       * @type {String}
       * @private
       */
      _pk      : {value: opts.pk},
      /**
       * @type {Object}
       * @private
       */
      _defaults: {value: opts.defaults || {}},
    });
    /**
     * @type {*}
     * @private
     */
    this._dsrc = opts.dsrc;
  }

  /**
   * Returns the DAO options
   * @returns {Object}
   */
  get opts() {
    return this._opts;
  }

  /**
   * The DAO entity primary key
   * @returns {String}
   */
  get entity() {
    return this._entity;
  }

  /**
   * The DAO entity primary key
   * @property pk
   * @returns {String}
   */
  get pk() {
    return this._pk;
  }

  /**
   * The DAO data source provider
   * @property dsrc
   * @returns {_dsrc}
   */
  get dsrc() {
    return this._dsrc;
  }

  /**
   * Create a new entity
   *
   * The data should contain all the fields of the Entity to be stored
   * Including the entity ID (Primary Key)
   *
   * @abstract
   * @param {Object} args The data used to create an Entity
   * @param {Object}  [opts] Creation options
   * @returns {Promise<{data: Object|null, meta:{}}>} The created Entity
   */
  async make(args, opts = {}) {
    throw Boom.notImplemented(
      `Not implemented for ${this.entity}.${this.pk} with ${Hoek.stringify(args)} and ${Hoek.stringify(opts)}`,
    );
  }

  /**
   * List all requested entries entities
   *
   * TODO@ion: Document ListFraming features
   *
   * @abstract
   * @param {*|{_:ListContext}} args
   * @param {Object}  [opts] Listing options
   * @returns {Promise<{data:Array, meta:{count:Number, total:Number}}>} An array of entities
   */
  async list(args, opts = {}) {
    throw Boom.notImplemented(
      `Not implemented for ${this.entity}.${this.pk} with ${Hoek.stringify(args)} and ${Hoek.stringify(opts)}`,
    );
  }

  /**
   * Load a single entity
   *
   * The `data` property should include the entity ID
   * Any other entity property is ignored
   *
   * @abstract
   * @param {Object|{id: *}}  args  The data used to locate the Entity
   * @param {{raw:boolean}}  [opts] Loading options
   * @returns {Promise<{data: Object|null, meta:{}}>} The modified Entity
   */
  async load(args, opts = {}) {
    throw Boom.notImplemented(
      `Not implemented for ${this.entity}.${this.pk} with ${Hoek.stringify(args)} and ${Hoek.stringify(opts)}`,
    );
  }

  /**
   * Modify an entity
   *
   * The `data` property should include the entity ID
   * and all entity properties that need modification
   *
   * REST PATCH model
   *
   * @abstract
   * @param {Object|{id: *}} args The data used to modify the Entity
   * @param {Object}  [opts] Editing options
   * @returns {Promise<{data: Object|null, meta:{}}>} The modified Entity
   */
  async edit(args, opts = {}) {
    throw Boom.notImplemented(
      `Not implemented for ${this.entity}.${this.pk} with ${Hoek.stringify(args)} and ${Hoek.stringify(opts)}`,
    );
  }

  /**
   * Delete an entity
   *
   * The `data` property should include the entity ID
   * Any other entity property is ignored
   *
   * @abstract
   * @param {Object|{id: *}} args The data used to delete the Entity.
   * @param {Object}  [opts] Killing options
   * @returns {Promise<{data: Number, meta:{}}>} The number of affected entries
   */
  async kill(args, opts = {}) {
    throw Boom.notImplemented(
      `Not implemented for ${this.entity}.${this.pk} with ${Hoek.stringify(args)} and ${Hoek.stringify(opts)}`,
    );
  }

  _onSilly(debug, context) {
    this.emit("silly", {entity: this.entity, debug, context});
  }

  _onDebug(debug, context) {
    this.emit("debug", {entity: this.entity, debug, context});
  }

  _onWarning(warning, context) {
    this.emit("warning", {entity: this.entity, warning, context});
  }

  _onError(error, context) {
    this.emit("error", {entity: this.entity, error, context});
  }

  _onException(ex, context, statusCode = 500) {
    if(500 <= statusCode) {
      this._onError(ex, context);
    } else {
      this._onDebug(ex, context);
    }
    throw Boom.boomify(ex, {statusCode: ex.statusCode || statusCode, override: false});
  }

}

module.exports = DAO;